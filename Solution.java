import java.util.HashMap;
import java.util.Map;
//glpat-cM6ocoMZ8K29TsRs-h3v


public class Solution {
    public int[] twoSum(int[] nums, int target) {
        //example nums = {2, 7, 9, 11}, taret = 9
        Map<Integer, Integer> num = new HashMap<>();
        int n = nums.length;

        for (int i = 0; i < n; i ++) {
            int place = target - nums[i];
            if (num.containsKey(place)) {
                return new int[] {num.get(place), i};
                }

                        num.put(nums[i], i);

            }
        return new int[]{};
        }

    }

